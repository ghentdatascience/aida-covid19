# README #

This repo will hold the source code for a addressing a number of research questions about Covid19 using 
the expertise in data science by the AIDA research team of Ghent University. 

* Brainstorm Notebooks (microsoft OneNote) can be found [here](https://ugentbe.sharepoint.com/teams/TW06_TMP00315/_layouts/OneNote.aspx?id=%2Fteams%2FTW06_TMP00315%2FGedeelde%20documenten%2FGeneral%2FStart02Teams_DDW&wd=target%28Covid19.one%7C4B877096-88AE-4529-8221-9F148660EB70%2F%29
onenote:https://ugentbe.sharepoint.com/teams/TW06_TMP00315/Gedeelde%20documenten/General/Start02Teams_DDW/Covid19.one#section-id={4B877096-88AE-4529-8221-9F148660EB70}&end)

## Phase I: deadline April 17 ##

* We will use the issue tracker to list a number of research/business questions and upvote the ones that seem most interesting to try to tackle with the team.
* This will lead to a priority list of RQs.
* Each research question will be split up into a number of subtasks which can be divided among the research team

## Phase II: deadline April 24 ##

* Fail fast: one week sprint to come up with some initial results 


