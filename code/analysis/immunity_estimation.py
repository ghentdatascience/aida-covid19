import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from .utils import sum_subset_age_groups, print_dict

# Source: Table 2 in https://www.cdc.gov/coronavirus/2019-ncov/hcp/planning-scenarios.html.
# -> ~6 days between infection and symptoms, ~15 days from infection to death (<65) and ~13 days for (>65).
# 13 days was chosen for the latter.
DAYS_INFECTION_TO_DEATH = 6 + 13

DAYS_SERIAL_INTERVAL = 5

INFECTION_FATALITY_RATES = {
    (0, 15, 'F'): 0,
    (0, 15, 'M'): 0,
    (15, 65, 'F'): 0.0002,
    (15, 65, 'M'): 0.0003,
    (65, 75, 'F'): 0.01,
    (65, 75, 'M'): 0.02,
    (75, 85, 'F'): 0.035,
    (75, 85, 'M'): 0.06,
    (85, 1000, 'F'): 0.18,
    (85, 1000, 'M'): 0.25,
}

EPIDEMIC_START = pd.to_datetime('2020-3-1')


def immunity_estimate(frame, countries=None):
    data_by_country = frame.groupby('country')
    if countries is None:
        countries = list(data_by_country.groups.keys())

    nb_countries = len(data_by_country)
    sqrt = np.ceil(np.sqrt(nb_countries))
    tightend = nb_countries <= (sqrt - 1) * sqrt
    nb_rows = int(sqrt - 1 if tightend else sqrt)
    nb_cols = int(sqrt)
    fig, axs = plt.subplots(nrows=nb_rows, ncols=nb_cols, sharex='all', sharey='all')
    nb_plots = nb_rows * nb_cols

    new_datas = []
    for i, country in enumerate(countries):
        ax = axs[i // (nb_rows + (1 if tightend else 0))][i % nb_cols]
        data = data_by_country.get_group(country)

        # Drop data with info on reported deaths.
        data = data[data['deaths_covid'].isnull()]

        # Drop data of people younger than 65 (too difficult to estimate).
        data = data[data['age_lb'] >= 65]

        # Group age group subsets if necessary and format the age groups back to strings.
        data = sum_subset_age_groups(data)
        data['age_group'] = data['age_lb'].astype(int).astype(str) + "_" + data['age_ub'].astype(int).astype(str)

        populations = {}
        infected_pops = {}
        excesses = []
        for subgroup, subgroup_data in data.groupby(['age_group', 'gender']):
            age_group, gender = subgroup
            age_lb, age_ub = (int(bound) for bound in age_group.split("_"))

            # We assume that the largest estimate of the population in the data is correct and therefore take the max.
            populations[subgroup] = subgroup_data['population'].max()

            # Use only data with more than 0 observed deaths and after the epidemic started.
            documented_idx = subgroup_data['observed_deaths'] > 0
            epidemic_started_idx = subgroup_data['date'] >= EPIDEMIC_START
            subgroup_data = subgroup_data[np.logical_and(documented_idx, epidemic_started_idx)]
            subgroup_data = subgroup_data.set_index('date')

            # Compute the incremental amount of excess deaths, and make negative values = 0.
            excess_deaths_increments = subgroup_data['observed_deaths'] - subgroup_data['expected_deaths']

            excess_deaths_increments_smooth = pd.Series(index=excess_deaths_increments.index,
                                                        data=triangle_smoothing(excess_deaths_increments))
            excess_deaths_increments = excess_deaths_increments_smooth

            # Discard data where the excess is 2 standard deviations smaller than 0.
            where_within_lower_std = excess_deaths_increments >= -subgroup_data['expected_d_std']
            excess_deaths_increments = excess_deaths_increments[where_within_lower_std]

            # Clip it to 0.
            excess_deaths_increments[excess_deaths_increments < 0] = 0

            # Save the excess deaths statistic.
            new_excess = pd.DataFrame(excess_deaths_increments, columns=['smooth_excess']).reset_index()
            new_excess['age_lb'] = age_lb
            new_excess['age_ub'] = age_ub
            new_excess['gender'] = gender
            excesses.append(new_excess)

            # Estimate the number of infected.
            infected_pop = excess_deaths_increments / INFECTION_FATALITY_RATES[(age_lb, age_ub, gender)]

            # Shift the dates to account for the fact that the deaths happen much later than the infection.
            infected_pop = infected_pop.shift(-DAYS_INFECTION_TO_DEATH, freq='D')
            infected_pops[subgroup] = infected_pop

        total_pop = np.sum(list(populations.values()))
        final_infected_pcts = {}
        total_infected = 0
        for subgroup, infected_pop in infected_pops.items():
            final_infected_pcts[subgroup] = infected_pop.sum() / populations[subgroup]
            total_infected = infected_pop + total_infected
        final_infected_pcts['all'] = total_infected.sum() / total_pop

        print("Final immunity percentages per group for country " + country)
        print_dict(final_infected_pcts)

        total_infected_pct = (total_infected / total_pop).cumsum()
        total_infected_pct.plot(ax=ax)

        # Set the y-axis ticks as percentages.
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
        ax.set_title(country)
        ax.set_ylabel("Immune 65+")

        R_e = calculate_effective_R(nb_infected_increments_weekly=total_infected)
        twinax = ax.twinx()
        twinax.set_ylim(bottom=0, top=3)
        # twinax.set_ylabel("infected diff")
        R_e.plot(ax=twinax, color='r')
        twinax.axhline(y=1, color='g', linestyle='-')

        R_e_frame = pd.DataFrame(R_e, columns=['R_e']).reset_index()
        R_e_frame = R_e_frame[R_e_frame['R_e'].notnull()]
        new_data = pd.concat(excesses + [R_e_frame], ignore_index=True)
        new_data['country'] = country
        new_datas.append(new_data)

    for i in range(nb_countries, nb_plots):
        ax = axs[i // nb_rows][i % nb_cols]
        ax.axis('off')

    # twin_axs[0].get_shared_y_axes().join(*twin_axs)
    plt.show()

    new_data = pd.concat(new_datas, ignore_index=True)
    new_data.to_csv('processed_data.csv')


def calculate_effective_R(nb_infected_increments_weekly):
    log_increments = nb_infected_increments_weekly.apply(np.log)

    # log_inc_diffs = []
    # for i in range(1, len(log_increments)-1):
    #     loc_inc_diff = (- log_increments[i-1] + log_increments[i + 1]) / 2
    #     log_inc_diffs.append(loc_inc_diff)
    #
    # relevant_dates = nb_infected_increments_weekly.index[1:-1]
    #
    # log_inc_diffs = pd.Series(data=log_inc_diffs, index=relevant_dates)

    # log_increments_smoothed = log_increments.rolling(3, win_type='triang').mean()
    # log_inc_diffs = log_increments_smoothed.diff()

    log_inc_diffs = log_increments.diff()

    # noinspection PyTypeChecker
    R_e = (log_inc_diffs * (DAYS_SERIAL_INTERVAL / 7)).apply(np.exp)
    return R_e


# Smoothe excess deaths signal using custom triangle smoothing.
def triangle_smoothing(series, window=None):
    if window is None:
        window = [0.5, 1, 0.5]

    window_sum = np.sum(window)
    half_len_floor = int((len(window) - 1) // 2)  # Assumed to have odd length.

    new_series = 0
    for win_i, win_coeff in enumerate(window):
        win_i_shifted = win_i - half_len_floor
        new_series += series.shift(win_i_shifted) * win_coeff
    new_series /= window_sum
    return new_series
