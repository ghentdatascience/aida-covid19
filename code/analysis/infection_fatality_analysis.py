import pandas as pd
import numpy as np

from .utils import sum_subset_age_groups, print_dict

# Source: Table 2 in https://www.cdc.gov/coronavirus/2019-ncov/hcp/planning-scenarios.html.
# -> ~6 days between infection and symptoms, ~15 days from infection to death (<65) and ~13 days for (>65).
# 15 days was chosen for the latter, since the data is weekly and not de-aggregated over the days.
DAYS_INFECTION_TO_IMMUNE = 14
DAYS_INFECTION_TO_DEATH = 6 + 15

# The first day that the total amount of reported deaths is larger than 50.
EPIDEMIC_START_DATES = {
    "GBRTENW": pd.to_datetime("2020-3-15"),
    "ESP": pd.to_datetime("2020-3-11"),
    "BEL": pd.to_datetime("2020-3-21"),
    "SWE": pd.to_datetime("2020-3-22"),
    "NLD": pd.to_datetime("2020-3-18")
}

TEST_DATES = {
    # Study was from 26th of April until 24th of May. This is the middle.
    "GBRTENW": pd.to_datetime("2020-5-10"),

    # The samples were gathered from 27th of April to the 11th of May. This is the middle.
    "ESP": pd.to_datetime('2020-5-4'),

    # There is little reported difference since the start of May.
    "BEL": pd.to_datetime('2020-5-3'),

    # The tests were conducted during the week from 27th of April to the 3rd of May. This is the middle.
    "SWE": pd.to_datetime('2020-4-30'),

    # There was a study from the 1st of April that was published on the 16th.
    "NLD": pd.to_datetime('2020-4-10')
}

IMMUNITY_TOTAL_PERCENTAGE = {
    "GBRTENW": 0.0678,

    # The percentage was ~4.7% since May. However, that was for mostly asymptomatic patients. We therefore estimate that
    # there are atleast 6% immune people, which assumes ~150,000 symptomatic covid cases.
    "BEL": 0.06,
    "SWE": 0.073,
    "NLD": 0.03
}


def infection_fatality_rate_by_age_group(frame, countries=None):
    if countries is None:
        countries = ['GBRTENW', 'ESP', 'BEL', 'SWE', 'NLD']

    data_by_country = frame.groupby('country')
    ifrs_by_country = {}
    for country in countries:
        data = data_by_country.get_group(country)

        # Drop data with info on reported deaths.
        data = data[data['deaths_covid'].isnull()]

        data = sum_subset_age_groups(data)
        data['age_group'] = data['age_lb'].astype(int).astype(str) + "_" + data['age_ub'].astype(int).astype(str)

        immunity_to_death_days = DAYS_INFECTION_TO_DEATH - DAYS_INFECTION_TO_IMMUNE
        corresponding_death_date = TEST_DATES[country] + pd.DateOffset(days=immunity_to_death_days)
        print("Using death statistics from date", corresponding_death_date)

        ifr_per_age_group = {}
        populations = {}
        total_excess = 0
        for subgroup, subgroup_data in data.groupby(['age_group', 'gender']):
            # We assume that the largest estimate of the population in the data is correct and therefore take the max.
            population = subgroup_data['population'].max()
            populations[subgroup] = population

            boolean_window = subgroup_data['date'].between(EPIDEMIC_START_DATES[country], corresponding_death_date)
            cum_stats = subgroup_data[boolean_window].sum()

            # The 'excess rate' is the odds to die in the observed period. It is also just a sum of the weekly rate,
            # since it is not possible to die in two separate weeks.
            excess_rate = cum_stats['death_rate'] - cum_stats['expected_death_rate']

            total_excess += cum_stats['observed_deaths'] - cum_stats['expected_deaths']

            if country == 'ESP':
                immunity_rate = subgroup_data['immune_covid'].max() / population
            elif country == 'SWE':
                if int(subgroup[0].split('_')[0]) >= 65:
                    immunity_rate = 0.027
                else:
                    immunity_rate = 0.067
            else:
                immunity_rate = IMMUNITY_TOTAL_PERCENTAGE[country]

            ifr_per_age_group[subgroup] = excess_rate / immunity_rate

        print("Total excess", country, total_excess)

        # Calculate the IFR per gender.
        weighted_sums = {'F': 0, 'M': 0}
        total_pop = {'F': 0, 'M': 0}
        for subgroup, ifr in ifr_per_age_group.items():
            weighted_sums[subgroup[1]] += ifr * populations[subgroup]
            total_pop[subgroup[1]] += populations[subgroup]
        for gender, weighted_sum in weighted_sums.items():
            ifr_per_age_group['all ' + gender] = weighted_sum / total_pop[gender]

        # The IFR overall.
        ifr_per_age_group['all'] = np.sum(list(weighted_sums.values())) / np.sum(list(total_pop.values()))

        ifrs_by_country[country] = ifr_per_age_group

    ifrs_as_df = pd.DataFrame(ifrs_by_country)
    ifrs_as_df.to_csv("ifrs_data.csv")

    # Format the results in a nicer table.
    from collections import OrderedDict
    table = []
    for country, ifrs in ifrs_by_country.items():
        ifr_dict = OrderedDict()
        ifr_dict['country'] = country
        for subgroup, ifr in ifrs.items():
            subgroup_str = subgroup[0] + " " + subgroup[1] if 'all' not in subgroup else subgroup
            ifr_dict[subgroup_str] = ifr
        ifr_dict['country '] = country
        table.append(ifr_dict)
    from tabulate import tabulate
    print(tabulate(table, headers='keys', tablefmt="github", floatfmt=".4f"))
