import pandas as pd


# Add smaller age groups into the age groups that contain them.
def sum_subset_age_groups(frame, attribute_columns=None):
    if attribute_columns is None:
        attribute_columns = ['date', 'gender']

    # Find all intervals.
    all_intervals = set()
    for lb, lb_data in frame.groupby('age_lb'):
        for ub in lb_data['age_ub'].unique():
            all_intervals.add((int(lb), int(ub)))
    all_intervals = list(all_intervals)

    # Map all small bounds into the largest bounds that contain them.
    larger_bounds_map = {}
    for i, query_bounds in enumerate(all_intervals):
        largest_bounds = None
        for j, other_bounds in enumerate(all_intervals):
            if query_bounds == other_bounds:
                continue
            if __bound_is_contained_by(query_bounds, other_bounds):
                if largest_bounds is None or __bound_is_contained_by(largest_bounds, other_bounds):
                    largest_bounds = other_bounds
        if largest_bounds is not None:
            larger_bounds_map[query_bounds] = largest_bounds

    # Map the larger bounds on the small bounds.
    pd.options.mode.chained_assignment = None
    for small_bounds, large_bounds in larger_bounds_map.items():
        bounds_match = ((frame['age_lb'] == small_bounds[0]) & (frame['age_ub'] == small_bounds[1])).values
        frame.iloc[bounds_match, frame.columns.get_loc('age_lb')] = large_bounds[0]
        frame.iloc[bounds_match, frame.columns.get_loc('age_ub')] = large_bounds[1]
    pd.options.mode.chained_assignment = 'warn'

    # Sum over the now duplicating bounds.
    frame = frame.groupby(['age_lb', 'age_ub'] + attribute_columns).sum().reset_index()

    return frame


def __bound_is_contained_by(smaller_bounds, larger_bounds):
    return smaller_bounds[0] >= larger_bounds[0] and smaller_bounds[1] <= larger_bounds[1]


def print_dict(dct):
    msg = ""
    for key, val in dct.items():
        msg += "\t" + str(key) + ": " + "%0.4f" % val + ",\n"
    msg = msg[:-2] + "\n"
    print(msg)
