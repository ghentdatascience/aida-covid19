
IMMUNITY_ESP_STATS = "Andalucía 9726 2,7 2,2 - 3,2 4607 2,7 2,2 - 3,3 5119 2,6 2,2 3,3\n\
                    Aragón 2748 4,9 3,8 - 6,3 1322 5,1 3,8 - 6,7 1426 4,8 3,5 - 6,5\n\
                    Asturias,_Principado_de 1545 1,8 1,3 - 2,5 713 1,9 1,1 - 3,3 832 1,7 1,2 - 2,5\n\
                    Balears,_Illes 1356 2,4 1,6 - 3,5 653 1,9 1,1 - 3,3 703 2,8 1,7 - 4,7\n\
                    Canarias 2324 1,8 1,1 - 2,8 1077 2,0 1,3 - 3,2 1247 1,5 0,8 - 2,9\n\
                    Cantabria 1504 3,2 2,1 - 5,0 731 2,3 1,4 - 3,8 773 4,0 2,4 - 6,7\n\
                    Castilla_y_León 6949 7,2 6,3 - 8,1 3389 6,6 5,6 - 7,6 3560 7,7 6,7 - 9,0\n\
                    Castilla-La_Mancha 5066 10,8 9,3 - 12,4 2485 9,9 8,4 - 11,6 2581 11,7 9,9 - 13,7\n\
                    Cataluña 6318 5,9 4,9 - 6,9 3018 6,0 4,9 - 7,4 3300 5,7 4,7 - 6,9\n\
                    Comunitat_Valenciana 4286 2,5 1,9 - 3,2 2064 2,8 2,1 - 3,7 2222 2,2 1,6 - 3,1\n\
                    Extremadura 2787 3,0 2,2 - 4,1 1389 2,8 1,9 - 4,1 1398 3,2 2,2 - 4,5\n\
                    Galicia 4070 2,1 1,7 - 2,6 1902 2,1 1,5 - 2,9 2168 2,2 1,6 - 3,0\n\
                    Madrid,_Comunidad_de 3185 11,3 9,8 - 13,0 1547 11,3 9,4 - 13,4 1638 11,3 9,6 - 13,3\n\
                    Murcia,_Región_de 1387 1,4 0,8 - 2,4 651 1,3 0,7 - 2,7 736 1,5 0,7 - 3,2\n\
                    Navarra,_Comunidad_Foral_de 1737 5,8 4,3 - 7,6 875 6,0 4,2 - 8,6 862 5,5 3,9 - 7,7\n\
                    País_Vasco 2830 4,0 3,1 - 5,2 1379 3,7 2,7 - 5,0 1451 4,3 3,2 - 5,9\n\
                    Rioja,_La 1323 3,3 2,4 - 4,4 642 3,4 2,3 - 5,1 681 3,1 2,0 - 5,0\n\
                    Ceuta 829 1,1 0,5 - 2,3 376 1,3 0,5 - 3,3 453 1,0 0,5 - 2,1\n\
                    Melilla 927 1,9 1,2 - 2,9 435 1,6 0,7 - 3,9 492 2,1 1,2 - 3,7"

CORRESPONDING_REGION_CODES = [
    "AN",
    "AR",
    "AS",
    "IB",
    "CN",
    "CB",
    "CL",
    "CM",
    "CT",
    "VC",
    "EX",
    "GA",
    "MD",
    "MC",
    "NC",
    "PV",
    "RI",
    "CE",
    "ML"
]


def infection_fatality_rate_by_region(frame):
    # Only perform this operation for the Spain data.
    data = frame.groupby('country').get_group('ESP').copy()

    # Sum over age/gender-specific information.
    data = data.drop(columns=['gender', 'age_lb', 'age_ub']).groupby(['date', 'region']).sum().reset_index()

    # Parse immunity statistics from string dump.
    immunity_percentages = [float(line.split(' ')[2].replace(",", ".")) / 100
                            for line in IMMUNITY_ESP_STATS.split('\n')]
    region_to_immunity_map = dict(zip(CORRESPONDING_REGION_CODES, immunity_percentages))
    region_id_to_name_map = dict(zip(CORRESPONDING_REGION_CODES,
                                     [line.split(' ')[0] for line in IMMUNITY_ESP_STATS.split('\n')]))

    # Calculate the death date that corresponds with the test date.
    immunity_to_death_days = DAYS_INFECTION_TO_DEATH - DAYS_INFECTION_TO_IMMUNE
    corresponding_death_date = TEST_DATES['ESP'] + pd.DateOffset(days=immunity_to_death_days)
    print("Using death statistics from date", corresponding_death_date)
    data = data[data['date'] <= corresponding_death_date]

    ifr_per_region = {}
    for region, region_data in data.groupby('region'):
        population = region_data['population'].max()
        if population < 1e6:
            continue

        data_in_window = region_data[region_data['date']
            .between(EPIDEMIC_START_DATES['ESP'], pd.to_datetime("2020-4-25"))]
        # data_in_window = data_in_window[data_in_window['observed_deaths'] > data_in_window['expected_deaths']]

        cum_stats = data_in_window.sum()
        excess_deaths = cum_stats['observed_deaths'] - cum_stats['expected_deaths']
        people_immune = population * region_to_immunity_map[region]
        ifr_per_region[region_id_to_name_map[region]] = excess_deaths / people_immune

    import pprint as pp
    pp.pprint(ifr_per_region)