import requests
import pandas as pd
import numpy as np

from .data_source import DataSource


class ApifyData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self):
        frames = []
        for func in [
            # self.__load_spain,
            self.__load_italy
        ]:
            frames.append(func())
        frame = pd.concat(frames, ignore_index=True, sort=False)
        return frame

    @staticmethod
    def __load_spain():
        json_data = requests.get("https://api.apify.com/v2/datasets/hxwow9BB75z8RV3JT/items?format=json&clean=1")
        data = json_data.json()
        dframe = pd.DataFrame(data)
        dframe = dframe.drop(columns=["sourceUrl", "readMe"])

        # Keep only the last rows per day, by using a truncated date column.
        dframe['date'] = dframe['lastUpdatedAtApify'].apply(pd.to_datetime).dt.date
        dframe = dframe.sort_values('lastUpdatedAtApify').drop_duplicates('date', keep='last')
        dframe = dframe.drop(columns=["lastUpdatedAtApify"])

        # The regions column only appears to have 'infected' counts, not deaths.
        # To compute per region, this pdf resource can help:
        # https://www.mscbs.gob.es/profesionales/saludPublica/ccayes/alertasActual/nCov-China/documentos/Actualizacion_89_COVID-19.pdf
        dframe = dframe.drop(columns=["regions"])

        # Go from cumulative counts to increases.
        dframe['infections_covid'] = dframe['infected'].astype(np.int).diff()
        dframe['deaths_covid'] = dframe['deceased'].astype(np.int).diff()
        dframe['recoveries_covid'] = dframe['recovered'].astype(np.int).diff()
        # dframe['hospitalizations_covid'] = dframe['hospitalised'].astype(np.int).diff()
        dframe = dframe.drop(columns=['infected', 'deceased', 'recovered', 'hospitalised'])
        dframe = dframe.drop(index=1)

        # Add country (spain).
        dframe['country'] = 'ESP'
        return dframe

    @staticmethod
    def __load_italy():
        json_data = requests.get("https://api.apify.com/v2/datasets/CUdKmb25Z3HjkoDiN/items?format=json&clean=1")
        data = json_data.json()
        dframe = pd.DataFrame(data)
        dframe = dframe.drop(columns=["sourceUrl", "readMe", 'lastUpdatedAtApify'])

        # Keep only the last rows per day, by using a truncated date column.
        dframe['date'] = dframe['lastUpdatedAtSource'].apply(pd.to_datetime).dt.date
        dframe = dframe.sort_values('lastUpdatedAtSource').drop_duplicates('date', keep='last')
        dframe = dframe.drop(columns=["lastUpdatedAtSource"])

        dframe = dframe.rename(columns={'newPositive': 'infections_covid'})
        # Note, number of recoveries can be calculated from "totalPositive".

        # Go from cumulative counts to increases.
        dframe['deaths_covid'] = dframe['deceased'].astype(np.int).diff()
        dframe['hospitalizations_covid'] = dframe['hospitalizedWithSymptoms'].astype(np.int).diff()
        dframe['discharges_covid'] = dframe['dischargedHealed'].astype(np.int).diff()
        dframe['icu_increase_covid'] = dframe['intensiveTherapy'].astype(np.int).diff()
        dframe = dframe.drop(columns=['hospitalizedWithSymptoms', 'intensiveTherapy', 'totalHospitalized',
                                      'homeInsulation', 'totalCurrentlyPositive', 'newCurrentlyPositive',
                                      'dischargedHealed', 'deceased', 'totalCases', 'tamponi', 'totalPositive'])
        dframe = dframe.drop(index=1)

        # Add country (spain).
        dframe['country'] = 'ITA'
        return dframe
