import pandas as pd
import numpy as np
import os

from .data_source import DataSource
from analysis.utils import sum_subset_age_groups

SCIENSANO_FILE = "COVID19BE_MORT.csv"


class BelgiumData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self, **kwargs):
        mort_data = pd.read_csv(os.path.join(self._data_path, "raw", SCIENSANO_FILE))
        mort_data = mort_data.rename(columns={"DATE": 'date', "SEX": 'gender', 'DEATHS': 'deaths_covid',
                                              "AGEGROUP": 'age_group'})

        # Replace the 'nan' values with more manageable 'missing' identifiers.
        mort_data['gender'] = mort_data['gender'].fillna('unknown')
        mort_data['age_group'] = mort_data['age_group'].fillna('unknown')

        # Sum over regions.
        mort_data = mort_data.drop(columns=['REGION']).groupby(['date', 'age_group', 'gender']).sum().reset_index()

        # Sum over all deaths as a check for later.
        total_deaths = mort_data['deaths_covid'].sum()

        all_genders = mort_data['gender'].unique()
        all_ages = mort_data['age_group'].unique()

        def pad_attributed(group):
            date = group['date'].min()
            new_rows = {
                'date': [],
                'gender': [],
                'age_group': [],
                'deaths_covid': []
            }
            for gender in all_genders:
                for age in all_ages:
                    if not np.any(np.logical_and(np.isin(group['gender'], gender), np.isin(group['age_group'], age))):
                        new_rows['date'].append(date)
                        new_rows['gender'].append(gender)
                        new_rows['age_group'].append(age)
                        new_rows['deaths_covid'].append(0)
            return pd.concat([group, pd.DataFrame(new_rows)], ignore_index=True)

        mort_data = mort_data.groupby('date').apply(pad_attributed).reset_index(drop=True)

        # Go from incrimental to cumsum.
        mort_data['deaths_covid'] = mort_data.groupby(['gender', 'age_group']).cumsum()

        # Distribute the completely unknown values over the completely labelled.
        def distribute(group):
            all_known = group[np.logical_and(group['gender'] != 'unknown', group['age_group'] != 'unknown')]
            nb_all_known = all_known['deaths_covid'].sum()
            none_known = group[np.logical_and(group['gender'] == 'unknown', group['age_group'] == 'unknown')]
            nb_none_known = none_known['deaths_covid'].sum()
            scale = nb_none_known / max(1, nb_all_known)

            new_rows = []
            for idx, row in group.iterrows():
                gender, age = row['gender'], row['age_group']
                if gender == 'unknown' or age == 'unknown':
                    continue

                new_val = row['deaths_covid']

                # Distribute the values for which the gender is known, but not the age group.
                only_gender_known = np.logical_and(group['gender'] == gender, group['age_group'] == 'unknown')
                nb_only_gender_known = group.loc[only_gender_known, 'deaths_covid'].sum()
                sum_gender_match = all_known.loc[all_known['gender'] == gender, 'deaths_covid'].sum()
                new_val += row['deaths_covid'] * nb_only_gender_known / max(1, sum_gender_match)

                # Distribute the values for which the age group is known, but not the gender.
                only_age_known = np.logical_and(group['gender'] == 'unknown', group['age_group'] == age)
                nb_only_age_known = group.loc[only_age_known, 'deaths_covid'].sum()
                sum_age_match = all_known.loc[all_known['age_group'] == age, 'deaths_covid'].sum()
                new_val += row['deaths_covid'] * nb_only_age_known / max(1, sum_age_match)

                # Distribute the values for which neither attribute is known.
                new_val += row['deaths_covid'] * scale

                row['deaths_covid'] = new_val
                new_rows.append(row)

            new_frame = pd.DataFrame(new_rows)
            return new_frame

        # Distribute the 'unknown' counts proportionally.
        mort_data = mort_data.groupby('date').apply(distribute).reset_index(drop=True)

        # Go back from cumsum to incrimental.
        first_date = mort_data['date'].min()
        first_date_vals = mort_data.loc[mort_data['date'] == first_date, 'deaths_covid']
        mort_data['deaths_covid'] = mort_data.drop(columns=['date']).groupby(['gender', 'age_group']).diff()
        mort_data.loc[mort_data['date'] == first_date, 'deaths_covid'] = first_date_vals

        # Check that the redistribution did not change the total counts.
        total_deaths_after_distribution = mort_data['deaths_covid'].sum()
        assert np.abs(total_deaths - total_deaths_after_distribution) < 1e-10

        mort_data['date'] = pd.to_datetime(mort_data['date'])
        mort_data = mort_data.set_index('date').groupby(['age_group', 'gender']).resample('W').sum().reset_index()

        # Map the small age groups to a larger one and sum the resulting duplicates.
        where_small_age_group = np.isin(mort_data['age_group'], ['0-24', '25-44', '45-64'])
        mort_data.loc[where_small_age_group, 'age_group'] = '0-64'
        mort_data = mort_data.groupby(['date', 'gender', 'age_group']).sum().reset_index()

        mort_data['age_group'] = mort_data['age_group'].apply(lambda x: x.replace('-', '_').replace('+', '_'))
        mort_data = self._map_textual_age_group(mort_data, add_one_to_ub=True)

        mort_data['country'] = 'BEL'
        return mort_data
