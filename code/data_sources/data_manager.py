import numpy as np
import pandas as pd

from .spain_data import SpainData
from .apify_data import ApifyData
from .italy_data import ItalyData
from .usa_data import USAData
from .ined_data import InedData
from .uk_data import UKData
from .germany_data import GermanyData
from .belgium_data import BelgiumData
from .hmd_data import HMDData

all_sources = [
    SpainData,
    # ApifyData,
    # ItalyData,
    # USAData,
    # InedData,
    # UKData,
    # GermanyData,
    BelgiumData,
    HMDData
]


class DataManager:
    def __init__(self, data_path, aggregate_history=False):
        self.__data_path = data_path

        self.__data_sources = None
        self.__full_dframe = None

        self.load_all_sources(aggregate_history)

    def load_all_sources(self, aggregate_history=False):
        full_dframe = StandardFrame.build()

        self.__data_sources = []
        for data_source_class in all_sources:
            new_source = data_source_class(data_path=self.__data_path)
            new_frame = new_source.load(aggregate_history=aggregate_history)
            full_dframe = StandardFrame.append_frame(full_dframe, new_frame)

            self.__add_data_source(new_source)
        self.__full_dframe = full_dframe

    def get_full_frame(self):
        return self.__full_dframe

    def get_sources(self):
        return self.__data_sources

    def __add_data_source(self, data_source):
        self.__data_sources.append(data_source)


class StandardFrame:
    # Note: every number-like dtype is a float, because pandas is bad at representing nullable integers.
    # The newest pandas versions support it with "pd.Int64Dtype()", but it is too buggy.
    column_dtypes = {
        'date': np.datetime64,
        'observed_deaths': np.float64,
        'expected_deaths': np.float64,
        'deaths_covid': np.float,
        # 'infections_covid': np.float,
        # 'recoveries_covid': np.float,
        # 'hospitalizations_covid': np.float,
        # 'discharges_covid': np.float,
        # 'icu_increase_covid': np.float,
        'immune_covid': np.float,
        'population': np.float,
        'country': np.object,
        'region': np.object,
        'age_lb': np.float,
        'age_ub': np.float,
        'gender': np.object
    }

    @staticmethod
    def build():
        frame = pd.DataFrame(columns=StandardFrame.column_dtypes)
        return frame

    @staticmethod
    def append_frame(main_frame, other_frame):
        if 'country' not in other_frame:
            raise ValueError("Did not find the country attribute. This makes grouping difficult!")

        if 'date' not in other_frame:
            raise ValueError("Did not find a 'date' attribute. Currently, we expect every row to be one 'day'.")

        # Merge rows and columns.
        merged_frame = pd.concat([main_frame, other_frame], ignore_index=True, sort=False)

        # Cast standard columns to the correct format.
        merged_frame = merged_frame.astype(StandardFrame.column_dtypes)

        # Normalize dates to midnight.
        merged_frame['date'] = merged_frame['date'].dt.normalize()
        return merged_frame
