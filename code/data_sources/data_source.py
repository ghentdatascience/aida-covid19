

class DataSource:
    def __init__(self, data_path, **kwargs):
        self._data_path = data_path

# INTERFACE METHODS
    def load(self, **kwargs):
        raise NotImplementedError

# SHARED CODE
    @staticmethod
    def _map_textual_age_group(df, add_one_to_ub=False):
        all_age_groups = df['age_group'].unique()
        lb_mapping = dict((age_group, age_group.split("_")[0]) for age_group in all_age_groups)
        df['age_lb'] = df['age_group'].map(lb_mapping).replace(to_replace="", value="0").astype(float)

        ub_mapping = dict((age_group, age_group.split("_")[1]) for age_group in all_age_groups)
        df['age_ub'] = df['age_group'].map(ub_mapping).replace(to_replace="", value="1000").astype(float)

        if add_one_to_ub:
            not_max = df['age_ub'] != 1000
            df.loc[not_max, 'age_ub'] = df.loc[not_max, 'age_ub'] + 1

        df = df.drop(columns=['age_group'])
        return df
