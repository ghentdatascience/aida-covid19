import pandas as pd
import os

from .data_source import DataSource

FILENAME = "mortality_org.xlsx"
AGE_GROUPS = ['0-14', '15-64', '65-74', '75-84', '85+']
HISTORY_WINDOW = (2014, 2019)  # Inclusive bounds of which years we consider to compute the expected death rates.


class HMDData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.__file_path = os.path.join(self._data_path, FILENAME)

    def load(self, aggregate_history=False, **kwargs):
        all_sheets = pd.read_excel(self.__file_path, sheet_name=None, skiprows=2)

        country_frames = []
        for sheet_name, frame in all_sheets.items():
            if sheet_name == "Description":
                continue
            country = sheet_name

            # Remove and rename some columns.
            frame = frame.drop(columns=['Total', 'Total.1', 'Split', 'SplitSex', 'Forecast'])
            frame = frame.rename(columns={"Country": 'country', "Sex": 'gender'})

            # Remove subtotals over gender.
            frame = frame[frame['gender'] != 'b']

            # To calculate the expected rates, we don't consider all years. Therefore, remove the irrelevant years.
            frame = frame[frame['Year'] >= HISTORY_WINDOW[0]]

            # Go from wide format (every age group a column) to long format (every age group a row).
            id_cols = ['Year', 'Week', 'gender', 'country']
            observed_deaths_frame = pd.melt(frame, id_vars=id_cols, var_name='age_group',
                                            value_vars=AGE_GROUPS, value_name='observed_deaths')

            age_groups_with_suffix = [age_group + ".1" for age_group in AGE_GROUPS]
            death_rates_frame = pd.melt(frame, id_vars=id_cols, var_name='age_group',
                                        value_vars=age_groups_with_suffix, value_name='death_rate')
            death_rates_frame['age_group'] = death_rates_frame['age_group'].apply(lambda x: x[:-2])

            # Set all identifier columns as a multiindex and then concatenate the two frames on that index.
            id_cols += ['age_group']
            frame = pd.concat([observed_deaths_frame.groupby(id_cols).sum(), death_rates_frame.groupby(id_cols).sum()],
                              axis=1)

            # The death rate is a weekly value and is the fraction of observed death counts, divided by the population,
            # times 52. In order to know what the propbability was to die in that week, we should therefore divide again
            # by 52. NOTE: it is unclear if this calculation is perfect for years <2020, but it is certainly sound for
            # the year 2020.
            frame['death_rate'] = frame['death_rate'] / 52

            if aggregate_history:
                # Set the 'year' part of the index to a column.
                frame = frame.reset_index(level='Year')

                # Take the mean over the relevant years by week.
                id_cols.remove('Year')
                history_frame = frame[frame['Year'].between(*HISTORY_WINDOW)].drop(columns='Year').reset_index()
                history_frame_mean = history_frame.groupby(id_cols).mean()
                std_devs = history_frame.groupby(id_cols).std()
                history_frame_mean = history_frame_mean.rename(columns={'observed_deaths': 'expected_deaths',
                                                                        'death_rate': 'expected_death_rate'})
                history_frame_std = std_devs.rename(columns={'observed_deaths': 'expected_d_std',
                                                             'death_rate': 'expected_dr_std'})

                # Merge the recent and expected data.
                recent_frame = frame[frame['Year'] > HISTORY_WINDOW[1]]
                frame = pd.concat([recent_frame, history_frame_mean, history_frame_std], axis=1)
                frame['Year'] = frame['Year'].max()

            frame = frame.reset_index()

            # For each week, take the last date in that week. It is different for some countries.
            last_day_of_week = 7  # Sunday
            if country == 'GBRTENW':
                last_day_of_week = 5  # Friday
            elif country == 'USA':
                last_day_of_week = 6  # Saturday
            frame['date'] = pd.to_datetime(frame['Year'].astype(int).astype(str) + "-" +
                                           frame['Week'].astype(int).astype(str) + "-" +
                                           str(last_day_of_week), format='%G-%V-%u')
            frame = frame.drop(columns=['Week', 'Year'])

            # Some final formatting.
            frame['gender'] = frame['gender'].apply(lambda x: x.upper())
            frame['age_group'] = frame['age_group'].apply(lambda x: x.replace("-", "_").replace("+", "_"))
            frame = self._map_textual_age_group(frame, add_one_to_ub=True)

            # This also means that we know what the population estimate is for 2020.
            frame['population'] = frame['observed_deaths'] / frame['death_rate']

            country_frames.append(frame)

        full_frame = pd.concat(country_frames, ignore_index=True)
        return full_frame
