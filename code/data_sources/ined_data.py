import pandas as pd
import os
import numpy as np

from .data_source import DataSource

FILENAME = "Deaths-Age-Sex_Covid-19_Spain_24-05.xlsx"


class InedData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self):
        full_dframe = pd.read_excel(os.path.join(self._data_path, "raw", FILENAME),
                                    sheet_name="MSCBS_Data", skiprows=3)

        # Drop population info.
        full_dframe = full_dframe.drop(columns=["Unnamed: "+str(i) for i in range(1, 13)])

        # Drop useless rows or sums.
        full_dframe = full_dframe.iloc[1:13]

        full_dframe = full_dframe.transpose()

        # Parse age groups.
        age_groups = full_dframe.iloc[0, 2:].values

        age_lbs = [0] + [int(age_group.split("-")[0]) for age_group in age_groups[1:-1]] \
                  + [age_groups[-1].split("+")[0]]
        age_ubs = [int(age_group.split("-")[1]) + 1 for age_group in age_groups[0:-1]] \
                  + [1000]

        full_dframe = full_dframe.iloc[1:]

        # Every 2nd row is a percentage and every 6th row is a sum. Filter them out.
        indices_to_keep = np.ones(len(full_dframe), dtype=np.bool)
        indices_to_keep[1::2] = False
        indices_to_keep[4::6] = False
        full_dframe = full_dframe.iloc[indices_to_keep]

        # Copy the date from the 'male' rows to the 'female' rows.
        full_dframe.iloc[1::2, 0] = full_dframe.iloc[0::2, 0].values

        # Go from cumulative counts to increases.
        full_dframe.iloc[::-2, 2:] = full_dframe.iloc[::-2, 2:].diff().values
        full_dframe.iloc[-2::-2, 2:] = full_dframe.iloc[-2::-2, 2:].diff().values

        # Iterate over rows and split the age groups over new rows. Can probably be done more efficiently.
        new_rows = []
        for i, row in full_dframe.iterrows():
            for j in range(len(age_groups)):
                columns_to_select = [0, 1, j+2]
                new_row = pd.DataFrame(row.iloc[columns_to_select]).T
                new_row = new_row.rename(columns={1: 'date', 2: 'gender', j+3: 'deaths_covid'})
                new_row['age_lb'] = age_lbs[j]
                new_row['age_ub'] = age_ubs[j]
                new_rows.append(new_row)
        full_dframe = pd.concat(new_rows, ignore_index=True)

        full_dframe['gender'] = full_dframe['gender'].replace('Males', "M")
        full_dframe['gender'] = full_dframe['gender'].replace('Females', "F")

        full_dframe['country'] = 'ESP'
        return full_dframe
