import pandas as pd
import os

from .data_source import DataSource

FILENAME = "2020-04-29_italy.csv"


class ItalyData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self):
        full_dframe = pd.read_csv(os.path.join(self._data_path, "clean", FILENAME),
                                         parse_dates=['dates'])
        full_dframe = full_dframe.rename(columns={'dates': 'date'})

        full_dframe = self._map_textual_age_group(full_dframe)

        full_dframe['country'] = 'ITA'
        return full_dframe
