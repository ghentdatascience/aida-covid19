import pandas as pd
import numpy as np
import os

from .data_source import DataSource
from analysis.utils import sum_subset_age_groups

MOMO_FILENAME = "2020-05-20_spain.csv"
UN_DATA = "UNdata_Export_20200520_110707909.csv"


class SpainData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self, **kwargs):
        # momo_stats = pd.read_csv(os.path.join(self._data_path, "clean", MOMO_FILENAME),
        #                          parse_dates=['death_date'])
        # momo_stats = momo_stats.rename(columns={'death_date': 'date'})
        # momo_stats = self._map_textual_age_group(momo_stats)

        # Read UN data
        un_stats = pd.read_csv(os.path.join(self._data_path, "raw", UN_DATA))

        # Drop last two rows (footnotes).
        un_stats = un_stats.iloc[:-2]

        # Drop all summation rows.
        un_stats = un_stats[~un_stats['Age'].str.contains('Total')]
        un_stats = un_stats[~un_stats['Age'].str.contains('-')]

        # Reformat some columns.
        un_stats['date'] = pd.to_datetime(un_stats['Year'])
        un_stats['gender'] = un_stats['Sex'].replace('Male', 'M').replace('Female', 'F')

        age_cleaned = un_stats['Age'].replace("100 +", "100").astype(float)
        un_stats['age_lb'] = age_cleaned
        un_stats['age_ub'] = age_cleaned.apply(lambda x: x + 1 if x < 100 else 1000)

        un_stats = un_stats.drop(columns=['Country or Area', 'Year', 'Sex', 'Area', 'Record Type', 'Reliability',
                                          'Source Year', 'Value Footnotes', 'Age'])
        un_stats = un_stats.rename(columns={"Value": 'population'})

        # Parse immunity stats from text dump.
        immune_dict = {
            "immune_pct_covid": [],
            "age_lb": [],
            "age_ub": [],
            "gender": []
        }
        for i, line in enumerate(IMMUNITY_ESP_PERCENTAGES.split('\n')):
            line_split = line.split(" ")
            for j, gender in enumerate(["M", "F"]):
                immune_dict['immune_pct_covid'].append(float(line_split[7 + j*5].replace(",", ".")) / 100)
                immune_dict['gender'].append(gender)
                immune_dict['age_lb'].append(float(line_split[0].split("-")[0]))
                immune_dict['age_ub'].append(float(line_split[0].split("-")[1]) + 1)
        immune_frame = pd.DataFrame(immune_dict)
        immune_frame['date'] = pd.to_datetime("2020-5-4")

        # Combine age groups from the population and immunity source.
        pop_immume_frame = pd.concat([un_stats, immune_frame], ignore_index=True)
        pop_immume_frame = sum_subset_age_groups(pop_immume_frame, attribute_columns=['gender'])

        # Calculate the number of immune people.
        pop_immune_grouped = pop_immume_frame.groupby(['age_lb', 'age_ub', 'gender']).sum()
        nb_immune = (pop_immune_grouped['population'] * pop_immune_grouped['immune_pct_covid']).reset_index()
        nb_immune = nb_immune.rename(columns={0: "immune_covid"})
        nb_immune['date'] = pd.to_datetime("2020-3-12")

        # full_dframe = pd.concat([momo_stats, nb_immune, un_stats], ignore_index=True)
        full_dframe = pd.concat([nb_immune], ignore_index=True)

        full_dframe['country'] = 'ESP'
        return full_dframe


IMMUNITY_ESP_PERCENTAGES = "0-0 268 1,1 0,3 - 3,8 131 1,6 0,3 - 7,9 137 0,5 0,1 - 2,1\n\
1-4 1693 2,2 1,4 - 3,6 865 2,0 1,1 - 3,6 828 2,5 1,2 - 4,9\n\
5-9 2857 3,0 2,3 - 4,1 1534 3,5 2,4 - 5,1 1323 2,5 1,6 - 3,9\n\
10-14 3425 3,9 3,1 - 4,9 1734 3,7 2,7 - 5,1 1691 4,2 3,1 - 5,6\n\
15-19 3221 3,8 3,0 - 4,9 1590 3,3 2,4 - 4,7 1631 4,3 3,2 - 5,7\n\
20-24 2805 4,5 3,5 - 5,7 1399 4,4 3,2 - 6,1 1406 4,6 3,2 - 6,4\n\
25-29 2606 4,8 3,7 - 6,1 1251 4,4 3,1 - 6,1 1355 5,1 3,6 - 7,3\n\
30-34 3050 3,8 2,9 - 4,9 1437 3,9 2,7 - 5,5 1613 3,7 2,6 - 5,2\n\
35-39 4000 4,6 3,8 - 5,6 1942 4,9 3,7 - 6,4 2058 4,4 3,4 - 5,6\n\
40-44 5174 5,3 4,5 - 6,2 2456 5,2 4,2 - 6,5 2718 5,3 4,3 - 6,5\n\
45-49 5330 5,7 4,9 - 6,7 2594 5,4 4,3 - 6,8 2736 6,0 4,9 - 7,2\n\
50-54 5263 5,8 4,9 - 6,9 2495 6,0 4,8 - 7,6 2768 5,6 4,6 - 6,7\n\
55-59 5187 6,1 5,2 - 7,2 2416 5,8 4,8 - 7,1 2771 6,3 5,2 - 7,8\n\
60-64 4560 5,9 5,0 - 7,0 2233 5,9 4,8 - 7,3 2327 5,9 4,8 - 7,4\n\
65-69 3568 6,2 5,1 - 7,4 1729 6,1 4,7 - 7,8 1839 6,2 4,9 - 7,8\n\
70-74 2931 6,9 5,7 - 8,3 1356 6,9 5,3 - 8,8 1575 6,9 5,4 - 8,8\n\
75-79 2161 6,1 4,8 - 7,7 999 7,0 5,1 - 9,4 1162 5,4 3,9 - 7,4\n\
80-84 1410 5,1 3,8 - 6,9 576 5,9 3,8 - 9,1 834 4,6 3,1 - 6,6\n\
85-89 968 5,6 3,8 - 8,2 371 5,2 2,7 - 9,5 597 5,9 3,6 - 9,3\n\
90-999 420 5,8 3,2 - 10,0 147 6,2 2,3 -15,5 273 5,6 2,7 -11,1"


POPULATION_BY_REGION_WIKIPEDIA_DUMP = \
    "1	1	 Andalusia	8,388,875	8,276,017	18.04%\n\
2	2	 Catalonia	7,518,903	7,463,471	15.94%\n\
3	3	 Community of Madrid	6,378,297	6,373,532	13.71%\n\
4	4	 Valencian Community	4,956,427	4,989,631	10.66%\n\
5	5	 Galicia	4,000,000	4,000,000	5.906463377%\n\
6	6	 Castile and León	2,495,689	2,547,408	5.37%\n\
7	7	 Basque Autonomous Community	2,167,166	2,179,532	4.66%\n\
8	9	 Canary Islands	2,114,845	2,045,168	4.55%\n\
9	8	 Castile-La Mancha	2,075,197	2,084,470	4.46%\n\
10	10	 Region of Murcia	1,461,803	1,453,545	3.14%\n\
11	11	 Aragon	1,311,301	1,344,184	2.82%\n\
12	12	 Extremadura	1,099,605	1,096,421	2.36%\n\
13	13	 Balearic Islands	1,115,841	1,077,103	2.4%\n\
14	14	 Asturias	1,058,975	1,077,103	2.28%\n\
15	15	 Navarre	636,450	633,017	1.37%\n\
16	16	 Cantabria	587,682	588,518	1.26%\n\
17	17	 La Rioja (Spain)	315,223	319,939	0.68%\n\
18	18	 Ceuta	84,674	79,294	0.18%\n\
19	19	 Melilla	83,870	76,047	0.18%"

CORRESPONDING_REGION_CODES = [
    "AN",
    "CT",
    "MD",
    "VC",
    "GA",
    "CL",
    "PV",
    "CN",
    "CM",
    "MC",
    "AR",
    "EX",
    "IB",
    "AS",
    "NC",
    "CB",
    "RI",
    "CE",
    "ML"
]


def format_pop_per_region():
    pop_dict = {
        "date": [],
        "population": [],
        "region": []
    }
    for i, line in enumerate(POPULATION_BY_REGION_WIKIPEDIA_DUMP.split('\n')):
        pop_dict['date'].append("2014-1-1")
        pop_dict['region'].append(CORRESPONDING_REGION_CODES[i])
        pop_dict['population'].append(float(line.split('\t')[3].replace(",", "")))
    pop_frame = pd.DataFrame(pop_dict)
    return pop_frame
