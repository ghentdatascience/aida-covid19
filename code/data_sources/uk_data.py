import pandas as pd
import os
import numpy as np

from .data_source import DataSource

WEEK = 19
FILENAME = "publishedweek" + str(WEEK) + "2020.xlsx"

HISTORIC_FILES = [
    "publishedweek522019.xls",
    "publishedweek522018withupdatedrespiratoryrow.xls",
    "publishedweek522017.xls",
    "publishedweek522016.xls",
    "publishedweek2015.xls"
]
START_YEAR = 2019
POPULATION_FILE = "Regional Population Estimates for England and Wales 1971-2015.xls"


class UKData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.__uk_file_path = os.path.join(self._data_path, "raw", "uk_data")

    def load(self):
        # # Read total deaths sheet.
        # total_info_frame = pd.read_excel(os.path.join(self.__uk_file_path, FILENAME),
        #                                  sheet_name="Weekly figures 2020", skiprows=5)
        # total_info_frame = self.__parse_frame(total_info_frame, row_start=37, nb_age_groups=20,
        #                                       val_name='observed_deaths')
        #
        # # Read the covid-related deaths sheet, by date of occurence.
        # reports_frame = pd.read_excel(os.path.join(self.__uk_file_path, FILENAME),
        #                               sheet_name="Covid-19 - Weekly occurrences", skiprows=5)
        # # Drop a summary column in the middle of the array.
        # reports_frame = reports_frame.drop(columns=['1 to '+str(WEEK)])
        # reports_frame = self.__parse_frame(reports_frame, row_start=27, nb_age_groups=20, val_name="deaths_covid")
        #
        # # Read the historic death counts.
        # historic_frames = []
        # for i, filename in enumerate(HISTORIC_FILES):
        #     year = START_YEAR - i
        #     sheet_name = 'Weekly figures ' + str(year)
        #     if year == 2015:
        #         sheet_name = 'Weekly Figures 2015'
        #     historic_frame = pd.read_excel(os.path.join(self.__uk_file_path, filename),
        #                                    sheet_name=sheet_name, skiprows=4)
        #     if year == 2015:
        #         historic_frame = historic_frame.rename(columns={'Week ended': 'placeholder_col'})
        #     historic_frame = self.__parse_frame(historic_frame, row_start=19, nb_age_groups=7,
        #                                         val_name="expected_deaths")
        #     historic_frame = self.__weekly_to_daily(historic_frame, number_columns=['expected_deaths'])
        #     historic_frames.append(historic_frame)
        # history_frame = pd.concat(historic_frames, sort=False, ignore_index=True)
        #
        # # Take the mean of all years.
        # history_frame['date'] = history_frame['date'].apply(lambda date: date.replace(year=START_YEAR + 1))
        # history_frame['expected_deaths'] = history_frame['expected_deaths'].astype(np.float)
        # history_frame = history_frame.groupby(['date', 'gender', 'age_lb', 'age_ub']).mean().reset_index()
        #
        # # Per gender and age group, sum the days over the same weeks as in the recent data.
        # history_frame = history_frame\
        #     .groupby([pd.Grouper(key='date', freq=pd.offsets.Week(weekday=4)), 'gender', 'age_lb', 'age_ub']) \
        #     .sum().reset_index()

        pop_data = self.__load_population_data()

        # full_dframe = pd.concat([total_info_frame, reports_frame, history_frame, pop_data],
        #                         sort=False, ignore_index=True)
        full_dframe = pop_data

        full_dframe['country'] = 'GBRTENW'
        return full_dframe

    @staticmethod
    def __parse_frame(frame, row_start, nb_age_groups, val_name):
        # Drop text column.
        try:
            frame = frame.drop(columns=["Week ended"])
        except KeyError:
            pass

        # Drop useless rows or sums. Keep only rows with counts for Males and Females, per age group.
        frame = frame.iloc[row_start:row_start + nb_age_groups * 2 + 2].reset_index(drop=True)
        frame = frame.drop([nb_age_groups, nb_age_groups + 1])

        frame = frame.transpose()

        # Set column names to first row.
        age_groups = frame.iloc[0][:nb_age_groups]
        age_gender_columns = pd.concat([age_groups + "_M", age_groups + "_F"]).values
        frame.columns = age_gender_columns
        frame = frame.iloc[1:].reset_index().rename(columns={'index': 'date'})

        # Our frame has rows for dates, and then many columns for every age group and gender.
        # Convert that to a frame of many rows, one for every subgroup of age and gender.
        frame = frame.melt(id_vars=['date'], var_name="subgroup", value_name=val_name)

        frame['gender'] = frame['subgroup'].apply(lambda x: x.split('_')[1])

        age_groups = age_groups.values
        age_lbs = [0] + [int(age_group.split("-")[0]) for age_group in age_groups[1:-1]] \
                  + [age_groups[-1].split("+")[0]]
        age_ubs = [1] + [int(age_group.split("-")[1]) + 1 for age_group in age_groups[1:-1]] \
                  + [1000]
        frame['age_lb'] = frame['subgroup'].replace(dict(zip(age_gender_columns, age_lbs + age_lbs)))
        frame['age_ub'] = frame['subgroup'].replace(dict(zip(age_gender_columns, age_ubs + age_ubs)))

        frame = frame.drop(columns=['subgroup'])
        frame = frame.sort_values(['date', 'gender', 'age_lb', 'age_ub'])
        return frame

    @staticmethod
    # TODO: optimize!
    def __weekly_to_daily(frame, number_columns):
        # Check that dates are sorted.
        assert np.all(frame['date'].values == frame['date'].sort_values().values)

        frame['date'] = frame['date'].astype(object)

        first_end = frame['date'].min()
        week_start = pd.to_datetime(str(first_end.year) + "-1-1")
        week_end = first_end
        date_range = pd.date_range(week_start, week_end)
        for i, row in frame.iterrows():
            # If we have moved to a new week, update the week start first.
            if row['date'] != week_end:
                week_start = week_end + pd.DateOffset(1)
                week_end = row['date']
                date_range = pd.date_range(week_start, min(week_end, pd.to_datetime(str(first_end.year) + "-12-31")))

            frame.at[i, 'date'] = list(date_range)
            for number_col in number_columns:
                frame.at[i, number_col] = row[number_col] / date_range.shape[0]

        frame = frame.explode('date')
        return frame

    def __load_population_data(self):
        frame = pd.read_excel(os.path.join(self.__uk_file_path, POPULATION_FILE),
                              sheet_name="GOR SYOA 1991-2015", skiprows=1)

        # Drop useless rows and columns.
        frame = frame.iloc[289:299].drop(
            columns=['Unnamed: 0', 'Unnamed: 1', ' ALL AGES', ' ALL AGES.1', ' ALL AGES.2'])

        # Sum over regions.
        frame = frame.sum()

        new_frame = []
        for index, pop_count in frame.items():
            index = str(index)
            index = index.replace("+", "")
            if '.1' in index:
                gender = 'F'
                index = index[:-2]
            else:
                gender = 'M'
            age_lb = int(index)
            new_row = {
                'date': [pd.to_datetime('2015-12-31')],
                'population': [pop_count],
                'age_lb': [age_lb],
                'age_ub': [age_lb + 1 if age_lb != 90 else 1000],
                'gender': [gender]
            }
            new_frame.append(pd.DataFrame(new_row))
        frame = pd.concat(new_frame, ignore_index=True)

        return frame
