import pandas as pd
import os

from .data_source import DataSource

FILENAME = "2020-05-05_usa.csv"


class USAData(DataSource):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def load(self):
        full_dframe = pd.read_csv(os.path.join(self._data_path, "clean",
                                               FILENAME),
                                  parse_dates=['dates'])
        full_dframe = full_dframe.rename(columns={'dates': 'date'})

        full_dframe = self._map_textual_age_group(full_dframe)

        full_dframe['country'] = 'USA'
        return full_dframe
