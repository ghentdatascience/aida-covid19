from os.path import dirname, abspath, join

import data_sources as dl
import visualization as vis
import analysis


DATA_PATH = join(dirname(abspath(__file__)), "..", "data")


def example_use():
    # Create a DataManager with knowledge of where the data is located. It will automatically load the data.
    data_mgr = dl.DataManager(data_path=DATA_PATH, aggregate_history=True)

    # Get the pandas dataframe that lists all data.
    full_dataframe = data_mgr.get_full_frame()

    # Print full dataframe.
    full_dataframe.to_csv('input_data.csv')

    # Example usage of visualization function.
    # vis.visualize_deaths_by_country(full_dataframe)
    # vis.visualize_deaths_by_age(full_dataframe, countries=['BEL'])

    analysis.infection_fatality_rate_by_age_group(full_dataframe)
    # analysis.infection_fatality_rate_by_region(full_dataframe)
    analysis.immunity_estimate(full_dataframe, countries=[
        'AUT', 'BEL', 'DEUTNP', 'DNK', 'ESP', 'FIN', 'FRATNP', 'GBRTENW', 'GBR_SCO', 'ITA', 'NLD', 'PRT', 'SWE', 'USA'])


if __name__ == '__main__':
    example_use()
