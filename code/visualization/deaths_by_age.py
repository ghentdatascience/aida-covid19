import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from datetime import datetime

from analysis.utils import sum_subset_age_groups, print_dict


def visualize_deaths_by_age(frame, countries=None):
    data_by_country = frame.groupby('country')
    if countries is None:
        countries = list(data_by_country.groups.keys())

    for country in countries:
        data = data_by_country.get_group(country)

        data = sum_subset_age_groups(data)

        data['age_group'] = data['age_lb'].astype(str) + "_" + data['age_ub'].astype(str)
        data = data[data['age_group'] != "nan_nan"]

        data_by_age_group = data.groupby('age_group', sort=False)
        nb_age_groups = len(data_by_age_group)

        # Some code to create a grid instead of a vertical stacking of plots.
        nb_plots = nb_age_groups
        nb_rows = int(np.ceil(np.sqrt(nb_plots)))
        nb_cols = int(np.ceil(np.sqrt(nb_plots)))
        fig, axs = plt.subplots(nrows=nb_rows, ncols=nb_cols, sharex='all')

        reporting_fracs = {}
        for i, (age_group, age_group_data) in enumerate(data_by_age_group):
            ax = axs[i // nb_rows][i % nb_cols]

            total_stats = age_group_data.groupby('date').sum()
            total_stats = total_stats[total_stats.index.to_series().between(pd.to_datetime("2020-3-1"),
                                                                            pd.to_datetime("2020-5-10"))]

            total_deaths = total_stats['observed_deaths']
            expected_deaths = total_stats['expected_deaths']

            deaths_covid = total_stats['deaths_covid']
            deaths_covid_excess = deaths_covid + expected_deaths

            deaths_covid_excess.plot(label='covid+expected', ax=ax)
            total_deaths.plot(ax=ax)
            expected_deaths.plot(ax=ax)

            ax.set_title(age_group)
            ax.legend()

            reporting_fracs[age_group] = deaths_covid.sum() / (total_deaths - expected_deaths).sum()

        print("Covid reporting factors for " + country + ":")
        print_dict(reporting_fracs)

        fig.suptitle(country + " by age group")
        plt.show()
