import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def visualize_deaths_by_country(frame):
    data_by_country = frame.groupby('country')

    nb_countries = len(data_by_country)
    nb_plots = nb_countries
    nb_rows = int(np.ceil(np.sqrt(nb_plots)))
    nb_cols = int(np.ceil(np.sqrt(nb_plots)))
    fig, axs = plt.subplots(nrows=nb_rows, ncols=nb_cols, sharex='all')

    for i, (country, country_data) in enumerate(data_by_country):
        country_data = country_data[country_data['date'] > pd.to_datetime("2020-1-1")]

        ax = axs[i // nb_rows][i % nb_cols]

        # country_data = country_data[country_data['observed_deaths'].notnull()]
        total_stats = country_data.groupby('date').sum()

        total_deaths = total_stats['observed_deaths']
        expected_deaths = total_stats['expected_deaths']

        total_deaths = total_deaths[total_deaths != 0]

        # deaths_covid = total_stats['deaths_covid']
        # deaths_covid_excess = expected_deaths + deaths_covid

        # deaths_covid_excess.plot(label='expected+covid', ax=ax)
        total_deaths.plot(ax=ax)
        expected_deaths.plot(ax=ax)

        ax.set_ylim(bottom=0)

        ax.set_title(country)

        if i == 0:
            ax.legend()

    for i in range(nb_countries, nb_plots):
        ax = axs[i // nb_rows][i % nb_cols]
        ax.axis('off')
    plt.show()
