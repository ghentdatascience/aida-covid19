#!/usr/bin/env python
# coding: utf-8

# In[1]:


import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objects as go

from dash.dependencies import Output,Input,State

import pandas as pd
import numpy as np
from pathlib import Path

import matplotlib.pyplot as plt
#%matplotlib inline

import time


# # Data: 
# - Covid Measures per country: https://covid19datahub.io/
# - Mortality data: [mortality.org](https://mortality.org)
# - Our World in data: [OWID](https://ourworldindata.org/coronavirus-data-explorer?yScale=log&zoomToSelection=true&deathsMetric=true&dailyFreq=true&aligned=true&smoothing=7&country=USA~GBR~CAN~BRA~AUS~IND~DEU~FRA~MEX~CHL~ZAF~DZA~COL) (not used for now)

# In[2]:


path_raw = "../data/raw/"
filename = "covid_datahub.csv"
prefix = "2020-06-15"

full_path = path_raw + prefix + filename

df_datahub = pd.read_csv(full_path)
df_datahub.head(n=3)



# In[3]:


df_datahub.columns


# ### Constants

# In[4]:


MEASURES = list(df_datahub.columns[10:21])
print(MEASURES)

df_countries = pd.DataFrame(df_datahub[['id', 'administrative_area_level_1']])
COUNTRIES = df_countries.drop_duplicates()
COUNTRIES.columns= ["code", "full_name"]

print(COUNTRIES.head(n=3))


# ### Local function library

# In[ ]:





# # 1. Layout
# 
# ### A. Div layout with 5 panes
# 

# In[5]:


div_row1_left = html.Div(id="row1_left", children=[],
                        style={
                            "border": "0px solid red", 
                            "height": "100%", "width": "49%",
                            "overflow": "hidden",
                            "float": "left"
                        }
                    )
div_row1_right = html.Div(id="row1_right", children=[],
                        style={
                            "border": "0px solid blue", 
                            "height": "100%", "width": "49%",
                            "overflow": "hidden",
                            "float": "left"
                        }
                    )

div_row1 = html.Div(id="row1", children=[div_row1_left, div_row1_right],
                        style={
                            "border": "0px solid black", 
                            "height": "60%", "width": "99%",
                            "overflow": "visible",
                        }
                    )


div_row2_left = html.Div(id="row2_left", children=[],
                        style={
                            "border": "0px solid red", 
                            "height": "100%", "width": "24%",
                            "overflow": "hidden",
                            "float": "left",
                            "padding-right": "5px"
                        }
                    )

div_row2_center = html.Div(id="row2_center", children=[],
                        style={
                            "border": "0px solid green", 
                            "height": "100%", "width": "49%",
                            "overflow": "visible",
                            "float": "left"
                        }
                    )

div_row2_right = html.Div(id="row2_right", children=[],
                        style={
                            "border": "0px solid black", 
                            "height": "100%", "width": "24%",
                            "overflow": "visible",
                            "float": "left"
                        }
                    )


div_row2 = html.Div(id="row_2", children=[div_row2_left, div_row2_center, div_row2_right],
                        style={
                            "border": "0px solid black", 
                            "height": "20%", "width": "99%",
                            "overflow": "visible",
                        }
                    )


# * layout: add controls in the second row

# ### B. Controls:
# 
# * selector: 2 countries to compare left and right  
# * measures: checkboxes
# * secondary plot with time shifter
# 
# #### create 2 dropdowns to select and add to dashboard

# In[6]:


#generate all country options for selector
options_country = [ {"label": row[1]['full_name'], 
             "value": row[1]['code'] 
            }     
           for row in COUNTRIES.iterrows()
          ]

select_country1 = dcc.Dropdown(
    id="country_dropdown1",
    options=options_country,
    value="NLD"
)

select_country2 = dcc.Dropdown(
    id="country_dropdown2",
    options=options_country,
    value="BEL"
)


# In[7]:


div_row2_left.children.append(select_country1)
div_row2_left.children.append(select_country2)


# #### create checkboxes for the measures

# In[8]:


opts_measures =[
        {"label": m.replace("_", " "), "value": m} for m in MEASURES
    ]

select_measure = dcc.Checklist(
    id="measures_checklist",
    options=opts_measures,
    value=MEASURES, #select all
    labelStyle={'display': 'inline-block'} #horizontal placement
)




# In[9]:


div_row2_center.children.append(select_measure)


# #### create time shift and select secondary plot

# In[10]:


div_row2_right_top = html.Div(id="row2_right_top", children=[],
                        style={
                            "border": "0px solid black", 
                            "height": "49%", "width": "99%",
                        }
                    )

div_row2_right_bottom = html.Div(id="row2_right_bottom", children=[],
                        style={
                            "border": "0px solid black", 
                            "height": "49%", "width": "99%",
                        }
                    )

div_row2_right.children.append(div_row2_right_top)
div_row2_right.children.append(div_row2_right_bottom)


# In[11]:


select_secondary_plot = dcc.Dropdown(
    id="select_secondary_plot",
    options=[
        {"label": 'R0 (TODO)', "value": "r0"},
        {"label": "Excess Mortality (TODO)", "value": "excess_mortality"},
        {"label": "New Cases", "value": "new_cases"},
        {"label": "New Covid Deaths", "value": "new_deaths"},
        {"label": "Hospitalized", "value": "hosp"},
        {"label": "Ventilator", "value": "vent"},
        {"label": "ICU", "value": "icu"},
        {"label": "Cumul. Cases", "value": "confirmed"},
        {"label": "Cumul. Covid Deaths", "value": "deaths"},
    ],
    value=["new_cases"],
    multi=True
)



div_row2_right_top.children.append(select_secondary_plot)


shift_button = html.Button("Time Shift!", id="shift_button")


shift_days = dcc.Input(id="shift_days", type='number', placeholder=5)

div_row2_right_bottom.children.append(shift_days)
div_row2_right_bottom.children.append(shift_button)


# # 2. add charts

# In[19]:


def generate_traces_areaplot(country, measures):
    all_traces = []
    mask_country = df_datahub['id'] == country
    df = df_datahub[mask_country]
    
    for measure in measures:
    
        trace1 = go.Scatter(x=df['date'],
                            y=df[measure],
                            name=measure,
                            mode='lines',
                            yaxis='y1',
                            stackgroup='one',
                            hoverinfo='x+y+text',
                            hovertext=measure,
                       )
        
        all_traces.append(trace1)
 

    return all_traces


def generate_traces_lineplot(country, what_2_plot):
    all_traces = []
    mask_country = df_datahub['id'] == country
    df = df_datahub[mask_country]
    
    colors = ['black', 'white', 'grey', 'blue', 'green']
    
    for i, pl in enumerate(what_2_plot):
        
        if pl == 'new_cases':
            column = df['confirmed'].diff().fillna(0).rolling(7).mean()
        elif pl == 'new_deaths':
            column = df['deaths'].diff().fillna(0).rolling(7).mean()

        else:
            column = df[pl].rolling(7).mean()
        
        
        trace = go.Scatter(x=df['date'],
                    y =  column,
                    name=pl,
                    mode='lines',
                    yaxis='y2',
                    line={
                        "color": colors[i],
                        "width": 5,

                    },
                    hoverinfo='x+y+text',
                    hovertext=pl
                  )
        all_traces.append(trace)
        

    
    
    return all_traces


def generate_traces_layout(country):
    layout = go.Layout(
        {
            "title": "Covid Measures "+country,
            'xaxis': {
                'title_text': 'Date',
                'showspikes': True,
                'spikethickness': 1,
                'spikecolor': 'black',
                'range': ["2020-02-01", "2020-07-01"]
            },
            "yaxis": {
                "title_text": "Strength",
                "side":       "left",
                'showspikes': True,
                'spikethickness': 1,
                'spikecolor': 'black',
                'range': [0,30]
            },
            "yaxis2": {
                "title_text": "",
                "side":       "right",
                "overlaying": 'y',
                'showspikes': True,
               'spikethickness': 1,
               'spikecolor': 'black'

            },

        },
        showlegend=False
    )
    
    return layout
    
    


# In[20]:


data_left = generate_traces_areaplot("BEL", MEASURES) +         generate_traces_lineplot("BEL", ["new_cases"])
    
data_right = generate_traces_areaplot("NLD", MEASURES) +         generate_traces_lineplot("NLD", ["new_cases"])

layout_left = generate_traces_layout("NLD")
layout_right = generate_traces_layout("BEL")


fig_left = go.Figure(data=data_left, layout=layout_left)
fig_right = go.Figure(data=data_right, layout=layout_right)

left_plot = dcc.Graph(id='left_plot', figure=fig_left)
right_plot = dcc.Graph(id='right_plot', figure=fig_right)

div_row1_left.children.append(left_plot)
div_row1_right.children.append(right_plot)


# In[ ]:





# In[ ]:





# # 3. Full Application

# In[14]:


app = dash.Dash()
div_style = {}

app.layout = html.Div([
    #html.H2('Covid Measures Analysis'),
    html.Div(id='full_viz', children=[div_row1, div_row2], style={
        "border": "0px solid green", 
        "height": "800px", "width": "1200px",
        "overflow": "visible"     
  
    })
    
    
], 
    style=div_style
)


# # 4. Add Interactions

# In[15]:


#input_button = Input(component_id='submit-button', component_property='n_clicks')
#state_field = State(component_id='number-in', component_property='value')
#output_h1 = Output(component_id='number-out', component_property='children')
#@app.callback(output_h1, [country_dropdown_left], [state_field])


# ### Interactions with Country dropdowns
# 
# 
# * 

# In[16]:


output_chart_left = Output(component_id='left_plot', component_property='figure')
output_chart_right = Output(component_id='right_plot', component_property='figure')
outputs = [output_chart_left, output_chart_right]

country_dropdown_left = Input(component_id='country_dropdown1', component_property='value')
country_dropdown_right = Input(component_id='country_dropdown2', component_property='value')
measures_check = Input(component_id='measures_checklist', component_property='value')
select_secondary_y = Input(component_id='select_secondary_plot', component_property='value')

inputs = [country_dropdown_left, country_dropdown_right, measures_check, select_secondary_y]

@app.callback(outputs, inputs)
def output_left(country_left, country_right, measures, secondary_y):
    
    data_left = generate_traces_areaplot(country_left, measures) +         generate_traces_lineplot(country_left, secondary_y)

    data_right = generate_traces_areaplot(country_right, measures) +         generate_traces_lineplot(country_right, secondary_y)
    
    layout_left = generate_traces_layout(country_left)
    layout_right = generate_traces_layout(country_right)

    return go.Figure(data=data_left, layout=layout_left),                 go.Figure(data=data_right, layout=layout_right)    



    


# In[ ]:





# In[17]:


if __name__ == '__main__':
    app.run_server(debug=True)


# In[23]:


get_ipython().system('jupyter nbconvert --to python Covid-Measures-DB.ipynb')


# In[ ]:





# In[ ]:




